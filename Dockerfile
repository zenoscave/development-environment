FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y --no-install-recommends \
  curl \
  git \
  python3 \
  python3-pip \
  python3-distutils \
  python3-pkg-resources
RUN rm -rf /var/lib/apt/lists/*

COPY ./scripts/env.sh home/dev/smoke-env
RUN chmod +x /home/dev/smoke-env

RUN groupadd -g 999 development && \
    useradd -r -u 999 -g development -G sudo dev

RUN chown -R dev:development /home/dev

USER dev

COPY ./scripts/bootstrap.sh /tmp/bootstrap
RUN bash /tmp/bootstrap
