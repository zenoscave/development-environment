#!/bin/bash
# nvm setup
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"
nvm use default

#python3 virtualenv wrapper setup
VIRTUALENVWRAPPER_PYTHON=$(which python3)
PATH="$PATH:$HOME/.local/bin"
source "$HOME/.local/bin/virtualenvwrapper.sh"
workon smoke-editor
